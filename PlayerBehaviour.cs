﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {
  /**
   * Выбор слоя поверхности для расчета прыжков и бега игрока
   **/  
  [SerializeField] private LayerMask ground;
  /**
   * Ускорение игрока по осям Ox и Oy
   **/  
  public Vector2 playerVelocity = new Vector2(5, 8);

  /**
   * Компоненты внутри объекта Player, которые нам понадобятся для работы
   **/ 
  private Rigidbody2D rigidBody2dComponent;
  private Collider2D collider2dComponent;
  private SpriteRenderer spriteRendererComponent; 
  private Animator animatorComponent;

  // Анимации                     0      1        2        3
  private enum AnimationState { idle, running, jumping, falling };
  // Текущая анимация
  private AnimationState currentAnimationState = AnimationState.idle;

  /**
   * Start
   **/
  private void Start() {
    // Получаем доступ к компонентам
    try {
      rigidBody2dComponent = gameObject.GetComponent<Rigidbody2D>();
      collider2dComponent = gameObject.GetComponent<Collider2D>();
      spriteRendererComponent = gameObject.GetComponent<SpriteRenderer>();
      animatorComponent = gameObject.GetComponent<Animator>();
    } catch (Exception error) {
      Debug.LogError(error);
    }
  }

  /**
  * Update
  **/
  private void Update() {
    // Обновляем позицию игрока каждый фрейм
    UpdatePlayerPosition();
  }

  // Обновляем местоположение игрока
  private void UpdatePlayerPosition() {
    // Получаем значения для горизонтального движения или прыжка, если соответствующая кнопка нажата
    float horizontaMoveInput = Input.GetAxis("Horizontal");
    float jumpInput = Input.GetAxis("Jump");

    // Движение влево
    if (horizontaMoveInput < 0) { 
      rigidBody2dComponent.velocity = new Vector2(-playerVelocity.x, rigidBody2dComponent.velocity.y);
      spriteRendererComponent.flipX = true;
    // Движение вправо  
    } else if (horizontaMoveInput > 0) { 
      rigidBody2dComponent.velocity = new Vector2(playerVelocity.x, rigidBody2dComponent.velocity.y);
      spriteRendererComponent.flipX = false;
    // Если персонаж стоит на земле и не двигается, отключаем инерцию
    } else if (collider2dComponent.IsTouchingLayers(ground)) {
      rigidBody2dComponent.velocity = Vector2.zero; 
    }

    // Если нажата клавиша прыжка и персонаж касается земли - прыгаем
    if (jumpInput > 0 && collider2dComponent.IsTouchingLayers(ground)) { 
      rigidBody2dComponent.velocity = new Vector2(rigidBody2dComponent.velocity.x, playerVelocity.y);
    }
  
    // Вызываем менеджер анимаций
    SetAnimationState();
  }

  /**
  *  Менеджер анимаций
  **/
  private void SetAnimationState() {
    /**
    * Персонаж касается земли 
    **/
    if (collider2dComponent.IsTouchingLayers(ground)) {
      // При помощи Mathf.Abs получаем модуль значения ускорения (если бежим влево, оно отрицательное)
      // Если оно больше 0.1 (не стоим на месте), то персонаж бежит
      if (Mathf.Abs(rigidBody2dComponent.velocity.x) > 0.1f) {
        currentAnimationState = AnimationState.running;
      } else {
      // Если меньше - стоим на месте  
        currentAnimationState = AnimationState.idle;
      }
    /**
    *  Персонаж не касается земли
    **/  
    } else {
      // Выбираем текущей анимацией прыжок
      currentAnimationState = AnimationState.jumping;

      if (currentAnimationState == AnimationState.jumping) {
        // Если ускорение уходит в отрицательное значение - персонаж падает вниз
        if (rigidBody2dComponent.velocity.y < .1f) {
          currentAnimationState = AnimationState.falling;
        }
      } else if (currentAnimationState == AnimationState.falling){
        // Если персонаж коснулся земли - он переходит в состояние спокойствия
        if (collider2dComponent.IsTouchingLayers(ground)) {
          currentAnimationState = AnimationState.idle;
        }
      }
    }

    // Изменяем значение state в аниматоре
    animatorComponent.SetInteger("state", (int)currentAnimationState);
  }  
}
