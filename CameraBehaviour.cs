﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour {
  // Объект, за которым будет следовать камера
  [Header("Target object")]
    public Transform targetObject; 
  
  // Ограничение движения камеры по левому нижнему и верхнему правому углам
  [Header("Camera position restrictions")]
    public Vector2 leftBottomLimitPosition = new Vector2(-11, -0.5f);
    public Vector2 rightTopLimitPosition  = new Vector2(23.5f, 6);

  /**
   * Update
   **/
  void Update() {
    // Обновляем позицию камеры каждый фрейм
    UpdateCameraPosition();
  }

  /**
   * Обновляем позицию камеры
   **/
  void UpdateCameraPosition() {
    try {
      transform.position = new Vector3(
        // Ограничение по Ox
        Mathf.Clamp(targetObject.position.x, leftBottomLimitPosition.x, rightTopLimitPosition.x), 
        // Ограничение по Oy
        Mathf.Clamp(targetObject.position.y, leftBottomLimitPosition.y, rightTopLimitPosition.y),
         /**
          *  Ограничение по Oz 
          *  Если камера исчезает, попробуйте поставить какое нибудь значение вместо transform.position.z (например, -10)
          **/
        transform.position.z 
      );
    } catch (Exception error) {
      Debug.LogError(error);
    }
  }
}
