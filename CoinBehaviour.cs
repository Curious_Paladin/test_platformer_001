﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinBehaviour : MonoBehaviour {
  /**
   * Слайдер для выбора длины вертикальной дистанции движения объекта
   **/  
  [Range(0.1f, 1.0f)] public float verticalMovementDistance = 0.30f;

  // Изначальное расположение объекта по оси Y 
  private float initialCoinVerticalPosition;

  /**
   * Start
   **/
  void Start() {
    // Получаем изначальное положение объекта по оси ординат 
    initialCoinVerticalPosition = transform.position.y;
  }

  /**
  * Update
  **/
  void Update() {
    // Обновляем позицию монеты каждый фрейм 
    CalculateCoinVerticalPosition();
  }

  // Рассчитываем текущее положение объекта
  void CalculateCoinVerticalPosition() {
    // Расчет значения Y
    float coinVerticalPosition = Mathf
      .Lerp(
        initialCoinVerticalPosition - (verticalMovementDistance / 2), 
        initialCoinVerticalPosition + (verticalMovementDistance / 2), 
        Mathf.PingPong(Time.time, 1)
      );

    // Присваиваем новое значение позиции объекта по оси Y 
    transform.position  = new Vector3(transform.position.x, coinVerticalPosition, transform.position.z);
  }

  // Активация триггера при попадании в него объекта
  void OnTriggerEnter2D(Collider2D collision) {
    /**
    * Проверяем, тэг объекта, активировавшего триггер
    * Если его тэг "Player", то условия выполнено
    **/
    if (collision.gameObject.tag == "Player") {
      Destroy(gameObject);
    }   
  }
}
